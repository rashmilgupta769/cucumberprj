package com.epam.project.Cucumber.steps;
import io.restassured.*;
import io.restassured.mapper.ObjectMapper;

import java.io.File;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import cucumber.TestContext;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
public class Hooks {
	
	TestContext testContext;

	public Hooks(TestContext context) {
		testContext = context;
	}

	@Before
	public void BeforeSteps() throws JsonMappingException, JsonProcessingException {
		JavascriptExecutor js=(JavascriptExecutor) testContext.getWebDriverManager().getDriver();
		js.executeScript("window.resizeTo(1024, 1024);");
		//js.executeScript("window.location = 'https://www.softwaretestingmaterial.com");
		WebDriver driver=testContext.getWebDriverManager().getDriver();
		driver.get("https://www.google.com/");
				 driver.switchTo().newWindow(WindowType.TAB);
				 driver.navigate().to("https://www.softwaretestingmaterial.com/");
		
		Response res=given().when().get("https://jsonplaceholder.typicode.com/todos/1");
		JsonPath jp=new JsonPath(res.asString());
		System.out.println(jp.getInt("userId"));
		com.fasterxml.jackson.databind.ObjectMapper obj=new com.fasterxml.jackson.databind.ObjectMapper();
		Typicode typ=obj.readValue(res.asString(), Typicode.class);
		String json=obj.writeValueAsString(typ);
		
		
		/*What all you can perform here
			Starting a webdriver
			Setting up DB connections
			Setting up test data
			Setting up browser cookies
			Navigating to certain page
			or anything before the test
		*/
	}

	@After
	public void AfterSteps(Scenario s) {
		if(s.isFailed()) {
			WebDriver driver=testContext.getWebDriverManager().getDriver();
			File  src=((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			//FileUtils.copy()
		}
	
		testContext.getWebDriverManager().quitDriver();
		
	}

}
