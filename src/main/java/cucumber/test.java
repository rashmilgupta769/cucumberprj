package cucumber;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class test {

	public static void main(String[] args) throws IOException, InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Rashmil_Gupta\\Documents\\workspace-spring-tool-suite-4-4.9.0.RELEASE\\Cucumber\\src\\test\\resources\\drivers\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		 driver.get("https://demos.devexpress.com/ASPxGridViewDemos/PagingAndScrolling/VirtualPaging.aspx");
	        driver.manage().window().maximize();

	        //Clicking on an element inside grid to get it into focus
	        driver.findElement(By.xpath("//*[@id='ContentHolder_ASPxGridView1_DXMainTable']//td[.='7/4/2018']")).click();

	        WebElement ele=null;
	        int flag=0;
	        int count=0;

	        do{
	            try{
	                //element to search for while scrolling in grid
	                ele = driver.findElement(By.xpath("//*[@id='ContentHolder_ASPxGridView1_DXMainTable']//td[.='5/6/2020']"));
	                flag=1;
	            } catch(Throwable e){
	                //scrolling the grid using the grid's xpath
	                driver.findElement(By.xpath("//*[@id=\"ContentHolder_ASPxGridView1_col0\"]/table/tbody/tr/td[1]")).sendKeys(Keys.PAGE_DOWN);
	                Thread.sleep(3000);
	            }
	        }while((flag==0) || ((++count)==250));

	        if(flag==1){
	            System.out.println("Element has been found.!!");
	        }else{
	            System.out.println("Element has not been found.!!");
	        }
		//driver.switchTo().parentFrame();
		driver.get("https://www.seleniumeasy.com/selenium-tutorials/find-broken-images-in-a-webpage-using-webdriver-java");
		List<WebElement> imgs=driver.findElements(By.tagName("img"));
		/*
		 * for(WebElement ele:imgs) { System.out.println(ele.getAttribute("src"));
		 * ele.getCssValue("color"); URL url=new URL(ele.getAttribute("src"));
		 * HttpURLConnection con=(HttpURLConnection) url.openConnection();
		 * con.connect(); if(con.getResponseCode()==200) {
		 * System.out.println("Valid image"); }else {
		 * System.out.println("Inavlid image"); } }
		 */
		Actions act=new Actions(driver);
		
		int a[] = {1,2,1,3,4,2,3};
		int n=5;
		int counter=0;
		List<Integer> list;
		for(int i=0;i<a.length;i++){
		int count1=0;
		list=new ArrayList<>();
		if(i+n>a.length) {
			break;
		}
		while(i<counter+n){
		if(!list.contains(a[i])){
			list.add(a[i]);
			count1++;
			}
		i++;
	
		}
		System.out.println("First list distinct ele " +count1+"  "+ list);
		
		counter=counter+1;
		i=counter-1;
		}

	}

}
