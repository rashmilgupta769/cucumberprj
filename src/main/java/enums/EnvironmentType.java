package enums;

public enum EnvironmentType {

	LOCAL(1),
	REMOTE(2);
	
	private int a;
	
	EnvironmentType(int a){
		this.a=a;
	}
	
	public int getA() {
		return a;
	}
}
class B
{
	
	public  void maintest(String args[]) {
		for(EnvironmentType e:EnvironmentType.values()) {
			System.out.println(e.getA());
		}
	}
}