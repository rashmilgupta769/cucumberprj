package manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import enums.DriverType;
import enums.EnvironmentType;

public class FileReaderManager {
	private Properties prop;
	private final String propertFile="C:\\Users\\Rashmil_Gupta\\Documents\\workspace-spring-tool-suite-4-4.9.0.RELEASE\\Cucumber\\config\\config.properties";
	
	private static FileReaderManager fileReaderManager;
	

	private FileReaderManager() {
		System.out.println("Inside File creation");
		File file=new File(propertFile) ;
		System.out.println(file);
		prop=new Properties();
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);
			prop.load(fis);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static FileReaderManager getFileReaderManager() {
	if(null==fileReaderManager) {
		fileReaderManager= new FileReaderManager();
	}
	return fileReaderManager;
	}
	
	public String getDriverPath() {
		String driverPath=prop.getProperty("driverPath");
		return driverPath;
	}
	public String getApplicationURL() {
		String url=prop.getProperty("url");
		return url;
	}
	
	public long getImplicitlyWait() {
		String implicitWait=prop.getProperty("implicitWait");
		return Long.parseLong(implicitWait);
	}
	
	public DriverType getBrowser() {
		String browser=prop.getProperty("browser");
		if(browser==null || browser.equals("chrome")) {
			return DriverType.CHROME;
		}else if (browser.equals("ie")) {
			return DriverType.INTERNETEXPLORER;
			
		}else {
			 throw new RuntimeException("Browser key not present");
		}
	}
	public EnvironmentType getEnvironment() {
		String env=prop.getProperty("environment");
		if(env.equals("local")) {
			return EnvironmentType.LOCAL;
		}else {
			return EnvironmentType.REMOTE;
		}
	}
	public String getReportConfigPath(){
		String reportConfigPath = prop.getProperty("reportConfigPath");
		if(reportConfigPath!= null) return reportConfigPath;
		else throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath");		
	}
}
