package manager;

import io.restassured.path.json.JsonPath;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import enums.DriverType;
import enums.EnvironmentType;

public class WebDriverManager {

	private WebDriver driver;
	private static DriverType driverType;
	private static EnvironmentType environmentType;
	private static final String CHROME_DRIVER_PROPERTY="webdriver.chrome.driver";

	public WebDriverManager() {
	
	driverType=FileReaderManager.getFileReaderManager().getBrowser();
	environmentType=FileReaderManager.getFileReaderManager().getEnvironment();
	}

	public WebDriver getDriver() {
	if(driver==null) {
		createDriver();
	}
	return driver;
	
	}

private WebDriver createDriver() {
	switch(environmentType) {
	case LOCAL:
		createLocalDriver();
		break;
	case REMOTE:
		break;
	}
	return driver;
}

private WebDriver createLocalDriver() {
	switch(driverType) {
	case CHROME:
		System.setProperty(CHROME_DRIVER_PROPERTY, FileReaderManager.getFileReaderManager().getDriverPath());
		driver=new ChromeDriver();
		break;
	case INTERNETEXPLORER:
		driver=new InternetExplorerDriver();
		break;
	}
	driver.manage().timeouts().implicitlyWait(FileReaderManager.getFileReaderManager().getImplicitlyWait(),TimeUnit.SECONDS);
	return driver;
	
}


public void quitDriver() {
	driver.close();
	driver.quit();
}
}
