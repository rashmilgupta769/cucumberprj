package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ProductListingPage {

	public ProductListingPage(WebDriver driver) {
		 PageFactory.initElements(driver, this);
		 }
		 
		 @FindBy(how = How.CSS, using = "button.single_add_to_cart_button") 
		 private WebElement btn_AddToCart;
		 
		 @FindBy(how = How.ID, using = "pa_color") 
		 private WebElement color;
		 
		 @FindBy(how = How.ID, using = "pa_size") 
		 private WebElement size;
		 
		 @FindAll(@FindBy(how = How.CSS, using = ".noo-product-inner"))
		 private List<WebElement> prd_List; 
		 
		 public void clickOn_AddToCart() {
			 Select sel=new Select(color);
			 sel.selectByVisibleText("White");
			 sel=new Select(size);
			 sel.selectByVisibleText("Medium");
			 btn_AddToCart.click();
		 }
		 
		 public void select_Product(int productNumber) {
		 prd_List.get(productNumber).click();
		 }
		 public String getProductName(int productNumber) {
			 return prd_List.get(productNumber).findElement(By.cssSelector("h3")).getText();
			 }
		 
		 
			 
}
