package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConfirmationPage {
	 WebDriver driver;
	 
	 public ConfirmationPage(WebDriver driver) {
	 this.driver = driver;
	     PageFactory.initElements(driver, this);
	 }
	 
	 @FindAll(@FindBy(how = How.CSS, using = ".order_item"))
	 private List<WebElement> prd_List; 
	 
	 public List<String> getProductNames() {
	 List<String> productNames = new ArrayList<>();
	 for(WebElement element : prd_List) {
	 productNames.add(element.findElement(By.cssSelector(".product-name")).getText());
	 }
	 return productNames;
	 }
	 public void test() {
		 driver.switchTo().defaultContent();
		 driver.switchTo().parentFrame();
		 Actions act=new Actions(driver);
		 act.contextClick();
		 act.doubleClick();
		 act.dragAndDrop(null, null);
		 driver.getWindowHandle();
		 driver.getWindowHandles();
		 driver.manage().window().maximize();
		 driver.manage().window().setSize(new Dimension(0, 0));
		 driver.switchTo().alert().accept();
		 driver.switchTo().alert().dismiss();
		 driver.switchTo().alert().sendKeys("");
		 WebElement ele = null;
		 ele.isDisplayed();
		 ele.isEnabled();
		 ele.isSelected();
		 driver.navigate().refresh();
		 driver.navigate().back();
		 driver.navigate().forward();
	Alert alert=driver.switchTo().alert();
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	WebDriverWait wait=new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.elementToBeClickable(ele));
//	Wait flu=new FluentWait<>(null)
	 }
}
